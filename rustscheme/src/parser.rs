use std::iter::Peekable;
use std::rc::Rc;

use crate::lexer::{Lexer, Token};
use crate::list;
use crate::SchemeVal;

#[derive(Debug)]
pub enum ParserError {
    IllegalToken,
    UnmatchedLParen,
    UnmatchedRParen,
}

#[derive(Debug)]
pub struct Parser<'a> {
    lexer: Peekable<Lexer<'a>>,
}

impl<'a> Parser<'a> {
    pub fn new(lexer: Lexer<'a>) -> Self {
        Parser {
            lexer: lexer.peekable(),
        }
    }

    fn parse_list(&mut self) -> Result<Rc<SchemeVal>, ParserError> {
        let mut vals = Vec::new();

        let mut t = self.lexer.peek().ok_or(ParserError::UnmatchedLParen)?;

        while *t != Token::RParen {
            vals.push(self.parse_expr()?);
            t = self.lexer.peek().ok_or(ParserError::UnmatchedLParen)?;
        }

        /* consume RParen */
        self.lexer.next();

        Ok(list(vals.as_slice()))
    }

    fn parse_expr(&mut self) -> Result<Rc<SchemeVal>, ParserError> {
        match self.lexer.next().unwrap() {
            Token::LParen => self.parse_list(),
            Token::RParen => Err(ParserError::UnmatchedRParen),
            Token::Ident(s) => Ok(Rc::new(SchemeVal::Symbol(s))),
            Token::Str(s) => Ok(Rc::new(SchemeVal::Str(s))),
            Token::Num(n) => Ok(Rc::new(SchemeVal::Number(n))),
            Token::Illegal => Err(ParserError::IllegalToken),
        }
    }

    pub fn parse(&mut self) -> Result<Vec<Rc<SchemeVal>>, ParserError> {
        let mut vals = Vec::new();

        while self.lexer.peek().is_some() {
            vals.push(self.parse_expr()?);
        }

        Ok(vals)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{number, symbol};

    #[test]
    fn parse1() {
        let input = "(define y 2)";
        let expected = list(&[symbol("define"), symbol("y"), number(2.0)]);

        let mut parser = Parser::new(Lexer::new(input));
        let parsed = parser.parse().unwrap();
        assert_eq!(parsed.len(), 1);
        assert_eq!(parsed[0], expected);
    }

    #[test]
    fn parse2() {
        let input = "(define (x) (y))";
        let expected = list(&[
            symbol("define"),
            list(&[symbol("x")]),
            list(&[symbol("y")]),
        ]);

        let mut parser = Parser::new(Lexer::new(input));
        let parsed = parser.parse().unwrap();
        assert_eq!(parsed.len(), 1);
        assert_eq!(parsed[0], expected);
    }
}
