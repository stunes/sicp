mod lexer;
mod parser;

use std::cell::RefCell;
use std::collections::BTreeMap;
use std::rc::Rc;

/// `SchemeError` represents an error that can be returned by the interpreter.
#[derive(Debug, Clone, PartialEq)]
enum SchemeError {
    /* TODO: proper error handling */
    UnboundVariable(String),
    /* TODO: associated data indicating syntax error */
    BadSyntax,
    NotAProcedure,
    ProcedureError(String),
    Other(String),
}

type Result<T> = std::result::Result<T, SchemeError>;

#[derive(Debug, Clone)]
pub struct Procedure {
    params: Vec<String>,
    body: Rc<SchemeVal>,
    env: Rc<RefCell<Environment>>,
}

impl PartialEq for Procedure {
    fn eq(&self, _other: &Self) -> bool {
        false
    }
}

/// Represents any Scheme value.
#[derive(Debug, Clone, PartialEq)]
pub enum SchemeVal {
    Number(f64),
    Str(String),
    Symbol(String),
    Pair {
        car: Rc<SchemeVal>,
        cdr: Rc<SchemeVal>,
    },
    EmptyList,
    True,
    False,
    Procedure(Procedure),
    PrimitiveProcedure(String),
}

impl SchemeVal {
    fn is_empty(&self) -> bool {
        matches!(self, SchemeVal::EmptyList)
    }

    fn is_pair(&self) -> bool {
        matches!(self, SchemeVal::Pair { car: _, cdr: _ })
    }

    fn is_symbol(&self) -> bool {
        matches!(self, SchemeVal::Symbol(_))
    }

    fn is_number(&self) -> bool {
        matches!(self, SchemeVal::Number(_))
    }

    fn car(&self) -> Rc<SchemeVal> {
        match self {
            SchemeVal::Pair { car, cdr: _ } => car.clone(),
            _ => panic!("car: value is not a Pair"),
        }
    }

    fn cdr(&self) -> Rc<SchemeVal> {
        match self {
            SchemeVal::Pair { car: _, cdr } => cdr.clone(),
            _ => panic!("cdr: value is not a Pair"),
        }
    }

    fn symbol_value(&self) -> &str {
        match self {
            SchemeVal::Symbol(s) => s,
            _ => panic!("symbol_value: value is not a Symbol"),
        }
    }

    fn as_number(&self) -> f64 {
        match self {
            SchemeVal::Number(n) => *n,
            _ => panic!("as_number: value is not a Number"),
        }
    }
}

pub fn list(elements: &[Rc<SchemeVal>]) -> Rc<SchemeVal> {
    match elements.len() {
        0 => Rc::new(SchemeVal::EmptyList),
        _ => Rc::new(SchemeVal::Pair {
            car: elements[0].clone(),
            cdr: list(&elements[1..]),
        }),
    }
}

fn unlist(val: Rc<SchemeVal>) -> Option<Vec<Rc<SchemeVal>>> {
    match &*val {
        SchemeVal::EmptyList => Some(vec![]),
        SchemeVal::Pair { car, cdr } => {
            let mut v = vec![car.clone()];
            match unlist(cdr.clone()) {
                Some(mut rest) => {
                    v.append(&mut rest);
                    Some(v)
                }
                None => None,
            }
        }
        _ => None,
    }
}

pub fn symbol<T>(name: T) -> Rc<SchemeVal>
where
    T: Into<String>,
{
    Rc::new(SchemeVal::Symbol(name.into()))
}

pub fn number(n: f64) -> Rc<SchemeVal> {
    Rc::new(SchemeVal::Number(n))
}

/// `Environment` maps string symbol names to Scheme values.
#[derive(Debug)]
struct Environment {
    vals: BTreeMap<String, Rc<SchemeVal>>,
    outer: Option<Rc<RefCell<Environment>>>,
}

impl Environment {
    /// Returns a new, empty environment with no parent environment.
    pub fn new() -> Self {
        Environment {
            vals: BTreeMap::new(),
            outer: None,
        }
    }

    #[cfg(test)]
    pub fn new_with_outer(outer: &Rc<RefCell<Environment>>) -> Self {
        Environment {
            vals: BTreeMap::new(),
            outer: Some(outer.clone()),
        }
    }

    /// Inserts a binding for symbol into this environment.
    pub fn insert(&mut self, symbol: String, val: Rc<SchemeVal>) {
        self.vals.insert(symbol, val);
    }

    /// Replaces an existing binding for symbol in the innermost environment
    /// in which it is already defined, or returns UnboundVariable if symbol is
    /// not bound.
    pub fn set(&mut self, symbol: String, val: Rc<SchemeVal>) -> Result<()> {
        if self.vals.contains_key(&symbol) {
            self.vals.insert(symbol, val);
            return Ok(());
        }

        match &mut self.outer {
            Some(e) => e.borrow_mut().set(symbol, val),
            None => Err(SchemeError::UnboundVariable(symbol)),
        }
    }

    /// Returns the value bound to symbol, or UnboundVariable if symbol is
    /// unbound.
    pub fn lookup(&self, symbol: &str) -> Result<Rc<SchemeVal>> {
        match self.vals.get(symbol) {
            Some(v) => Ok(v.clone()),
            None => match &self.outer {
                Some(e) => e.borrow().lookup(symbol),
                None => Err(SchemeError::UnboundVariable(symbol.into())),
            },
        }
    }

    /// Returns the value bound to a symbol, searching only in the current frame
    /// of the environment and not searching in any parent frames.
    #[cfg(test)]
    pub fn lookup_only(&self, symbol: &str) -> Result<Rc<SchemeVal>> {
        match self.vals.get(symbol) {
            Some(v) => Ok(v.clone()),
            None => Err(SchemeError::UnboundVariable(symbol.into())),
        }
    }
}

fn new_root_env() -> Rc<RefCell<Environment>> {
    let mut e = Environment::new();
    e.insert(
        "+".to_string(),
        Rc::new(SchemeVal::PrimitiveProcedure("+".to_string())),
    );
    e.insert(
        "-".to_string(),
        Rc::new(SchemeVal::PrimitiveProcedure("-".to_string())),
    );
    e.insert(
        "*".to_string(),
        Rc::new(SchemeVal::PrimitiveProcedure("*".to_string())),
    );
    e.insert(
        "=".to_string(),
        Rc::new(SchemeVal::PrimitiveProcedure("=".to_string())),
    );
    Rc::new(RefCell::new(e))
}

fn extend_env(
    e: Rc<RefCell<Environment>>,
    symbols: Vec<String>,
    vals: Vec<Rc<SchemeVal>>,
) -> Result<Environment> {
    if symbols.len() != vals.len() {
        return Err(SchemeError::Other(
            "extend_env: symbols.len() != values.len()".into(),
        ));
    }

    let mut m = BTreeMap::new();
    for (s, v) in symbols.into_iter().zip(vals) {
        m.insert(s, v);
    }

    Ok(Environment {
        vals: m,
        outer: Some(e),
    })
}

/// Evaluates a quote form, where `v` is the cdr of the original form.
fn eval_quoted(v: Rc<SchemeVal>) -> Result<Rc<SchemeVal>> {
    match &*v {
        SchemeVal::Pair { car, cdr: _ } => Ok(car.clone()),
        _ => Err(SchemeError::BadSyntax),
    }
}

/// Evaluates a define form, where `v` is the cdr of the original form.
fn eval_definition(
    v: Rc<SchemeVal>,
    env: Rc<RefCell<Environment>>,
) -> Result<Rc<SchemeVal>> {
    if !v.is_pair() {
        return Err(SchemeError::BadSyntax);
    }

    if !v.cdr().is_pair() {
        return Err(SchemeError::BadSyntax);
    }

    if !v.cdr().cdr().is_empty() {
        return Err(SchemeError::BadSyntax);
    }

    let var = v.car();
    let val = v.cdr().car();

    if let SchemeVal::Symbol(s) = &*var {
        let evaled = eval(val, env.clone())?;
        env.borrow_mut().insert(s.to_string(), evaled);
        Ok(symbol("ok"))
    } else {
        Err(SchemeError::BadSyntax)
    }
}

/// Evaluates a set! form, where `v` is the cdr of the original form.
fn eval_assignment(
    v: Rc<SchemeVal>,
    env: Rc<RefCell<Environment>>,
) -> Result<Rc<SchemeVal>> {
    if !v.is_pair() {
        return Err(SchemeError::BadSyntax);
    }

    if !v.cdr().is_pair() {
        return Err(SchemeError::BadSyntax);
    }

    if !v.cdr().cdr().is_empty() {
        return Err(SchemeError::BadSyntax);
    }

    let var = v.car();
    let val = v.cdr().car();

    if let SchemeVal::Symbol(s) = &*var {
        let evaled = eval(val, env.clone())?;
        match env.borrow_mut().set(s.to_string(), evaled) {
            Ok(_) => Ok(symbol("ok")),
            Err(e) => Err(e),
        }
    } else {
        Err(SchemeError::BadSyntax)
    }
}

/// Evaluates a conditional without an alternate. Returns SchemeVal::False if
/// the predicate is false.
fn eval_if_no_altern(
    pred: Rc<SchemeVal>,
    conseq: Rc<SchemeVal>,
    env: Rc<RefCell<Environment>>,
) -> Result<Rc<SchemeVal>> {
    let evaled_pred = eval(pred, env.clone());

    match evaled_pred {
        Err(_) => evaled_pred,
        Ok(v) => match *v {
            SchemeVal::False => Ok(Rc::new(SchemeVal::False)),
            _ => eval(conseq, env),
        },
    }
}

/// Evaluates a conditional with an alternate.
fn eval_if_altern(
    pred: Rc<SchemeVal>,
    conseq: Rc<SchemeVal>,
    altern: Rc<SchemeVal>,
    env: Rc<RefCell<Environment>>,
) -> Result<Rc<SchemeVal>> {
    let evaled_pred = eval(pred, env.clone());

    match evaled_pred {
        Err(_) => evaled_pred,
        Ok(v) => match *v {
            SchemeVal::False => eval(altern, env),
            _ => eval(conseq, env),
        },
    }
}

/// Evaluates a conditional, where `v` is the cdr of the original form.
fn eval_if(
    v: Rc<SchemeVal>,
    env: Rc<RefCell<Environment>>,
) -> Result<Rc<SchemeVal>> {
    /*
     * car v: predicate
     * cadr v: consequent
     * caddr v: alternate
     */
    if !v.is_pair() {
        return Err(SchemeError::BadSyntax);
    }

    if !v.cdr().is_pair() {
        return Err(SchemeError::BadSyntax);
    }

    let pred = v.car();
    let conseq = v.cdr().car();

    if v.cdr().cdr().is_empty() {
        eval_if_no_altern(pred, conseq, env)
    } else {
        let altern = v.cdr().cdr().car();
        if !v.cdr().cdr().cdr().is_empty() {
            Err(SchemeError::BadSyntax)
        } else {
            eval_if_altern(pred, conseq, altern, env)
        }
    }
}

/// Evaluates a lambda form, that is, a form creating a new lambda, not a lambda
/// application. `v` is the cdr of the original form.
fn eval_lambda(
    v: Rc<SchemeVal>,
    env: Rc<RefCell<Environment>>,
) -> Result<Rc<SchemeVal>> {
    /*
     * car v: parameters
     * cdr v: body
     */
    if !v.is_pair() {
        return Err(SchemeError::BadSyntax);
    }

    let params = v.car();
    let body = v.cdr();

    match unlist(params) {
        Some(param_list) => {
            if param_list.iter().all(|x| x.is_symbol()) {
                let param_symbols = param_list
                    .iter()
                    .map(|x| x.symbol_value().to_string())
                    .collect();
                Ok(Rc::new(SchemeVal::Procedure(Procedure {
                    params: param_symbols,
                    body,
                    env,
                })))
            } else {
                Err(SchemeError::BadSyntax)
            }
        }
        None => Err(SchemeError::BadSyntax),
    }
}

fn eval_sequence(
    vals: &[Rc<SchemeVal>],
    env: Rc<RefCell<Environment>>,
) -> Result<Rc<SchemeVal>> {
    match vals.len() {
        0 => Ok(symbol("ok")),
        1 => eval(vals[0].clone(), env),
        _ => {
            let r = eval(vals[0].clone(), env.clone());
            if r.is_err() {
                return r;
            }
            eval_sequence(&vals[1..], env)
        }
    }
}

fn eval_begin(
    v: Rc<SchemeVal>,
    env: Rc<RefCell<Environment>>,
) -> Result<Rc<SchemeVal>> {
    if !v.is_empty() && !v.is_pair() {
        return Err(SchemeError::BadSyntax);
    }

    match unlist(v) {
        Some(vals) => eval_sequence(&vals, env),
        None => Err(SchemeError::BadSyntax),
    }
}

fn eval_primitive(
    name: &str,
    params: &[Rc<SchemeVal>],
) -> Result<Rc<SchemeVal>> {
    match name {
        "+" => {
            if params.iter().all(|p| p.is_number()) {
                let n = params.iter().map(|p| p.as_number()).sum();
                Ok(Rc::new(SchemeVal::Number(n)))
            } else {
                Err(SchemeError::ProcedureError(
                    "+: non-numeric argument".into(),
                ))
            }
        }
        "-" => {
            if params.iter().all(|p| p.is_number()) {
                if params.is_empty() {
                    Ok(number(0.0))
                } else {
                    let mut acc = params[0].as_number();
                    for n2 in params.iter().skip(1) {
                        acc -= n2.as_number();
                    }
                    Ok(number(acc))
                }
            } else {
                Err(SchemeError::ProcedureError(
                    "-: non-numeric argument".into(),
                ))
            }
        }
        "*" => {
            if params.iter().all(|p| p.is_number()) {
                if params.is_empty() {
                    Ok(number(1.0))
                } else {
                    let mut acc = 1.0;
                    for n in params.iter() {
                        acc *= n.as_number();
                    }
                    Ok(number(acc))
                }
            } else {
                Err(SchemeError::ProcedureError(
                    "*: non-numeric argument".into(),
                ))
            }
        }
        "=" => {
            if params.len() == 2 {
                if params[0] == params[1] {
                    Ok(Rc::new(SchemeVal::True))
                } else {
                    Ok(Rc::new(SchemeVal::False))
                }
            } else {
                Err(SchemeError::ProcedureError("=: need two arguments".into()))
            }
        }
        _ => Err(SchemeError::NotAProcedure),
    }
}

/// Evaluates an application. `car` and `cdr` are the car and cdr of the
/// original form.
fn eval_application(
    oper_expr: Rc<SchemeVal>,
    cdr: Rc<SchemeVal>,
    env: Rc<RefCell<Environment>>,
) -> Result<Rc<SchemeVal>> {
    /* eval operands in this environment */
    let param_exprs = match unlist(cdr) {
        Some(vals) => vals,
        None => return Err(SchemeError::BadSyntax),
    };

    let params_evaled: Vec<_> = param_exprs
        .iter()
        .map(|p| eval(p.clone(), env.clone()))
        .collect();
    if params_evaled.iter().any(|p| p.is_err()) {
        let p = params_evaled
            .iter()
            .find(|p| p.is_err())
            .unwrap()
            .clone()
            .unwrap_err();
        return Err(p);
    }
    let params_evaled: Vec<Rc<SchemeVal>> = params_evaled
        .iter()
        .map(|p| p.as_ref().unwrap().clone())
        .collect();

    /* eval operator and get procedure */
    let proc = eval(oper_expr, env)?;

    match &*proc {
        SchemeVal::Procedure(p) => {
            let eval_env = match extend_env(
                p.env.clone(),
                p.params.clone(),
                params_evaled,
            ) {
                Ok(e) => e,
                Err(e) => return Err(e),
            };
            let eval_env = Rc::new(RefCell::new(eval_env));

            match unlist(p.body.clone()) {
                Some(vals) => eval_sequence(&vals, eval_env),
                None => Err(SchemeError::Other(
                    "procedure body is not a list".into(),
                )),
            }
        }
        SchemeVal::PrimitiveProcedure(name) => {
            eval_primitive(&name, &params_evaled)
        }
        _ => Err(SchemeError::NotAProcedure),
    }
}

/// Evaluates a pair expression.
fn eval_pair(
    car: Rc<SchemeVal>,
    cdr: Rc<SchemeVal>,
    env: Rc<RefCell<Environment>>,
) -> Result<Rc<SchemeVal>> {
    match &*car {
        SchemeVal::Symbol(s) => match s.as_str() {
            "quote" => eval_quoted(cdr),
            "define" => eval_definition(cdr, env),
            "set!" => eval_assignment(cdr, env),
            "if" => eval_if(cdr, env),
            "lambda" => eval_lambda(cdr, env),
            "begin" => eval_begin(cdr, env),
            _ => eval_application(car, cdr, env),
        },
        SchemeVal::Pair { car: _, cdr: _ } => eval_application(car, cdr, env),
        _ => unimplemented!(),
    }
}

/// Evaluates `v` as Scheme code and returns the resulting value.
fn eval(
    v: Rc<SchemeVal>,
    env: Rc<RefCell<Environment>>,
) -> Result<Rc<SchemeVal>> {
    match &*v {
        SchemeVal::True => Ok(v),
        SchemeVal::False => Ok(v),
        SchemeVal::Number(_) => Ok(v),
        SchemeVal::Str(_) => Ok(v),
        SchemeVal::Symbol(s) => env.borrow().lookup(&s),
        SchemeVal::EmptyList => Ok(v),
        SchemeVal::Pair { car, cdr } => {
            eval_pair(car.clone(), cdr.clone(), env)
        }
        SchemeVal::Procedure(_) => unimplemented!(),
        SchemeVal::PrimitiveProcedure(_) => unimplemented!(),
    }
}

fn main() {
    println!("Hello, world!");
}

#[cfg(test)]
mod tests {
    use super::*;
    use lexer::Lexer;
    use parser::Parser;

    fn new_env() -> Rc<RefCell<Environment>> {
        Rc::new(RefCell::new(Environment::new()))
    }

    #[test]
    fn eval_number() {
        let e = new_env();
        let n = number(42.0);
        let evaled = eval(n.clone(), e);
        assert_eq!(*evaled.unwrap(), *n);
    }

    #[test]
    fn eval_string() {
        let e = new_env();
        let s = Rc::new(SchemeVal::Str("Hello, world!".to_string()));
        let evaled = eval(s.clone(), e);
        assert_eq!(*evaled.unwrap(), *s);
    }

    #[test]
    fn eval_symbol() {
        let e = new_env();
        let s = Rc::new(SchemeVal::Str("Hello, world!".to_string()));
        e.borrow_mut().insert("foo".to_string(), s.clone());

        let smb = symbol("foo");
        let evaled = eval(smb, e);

        assert_eq!(*evaled.unwrap(), *s)
    }

    #[test]
    fn eval_symbol_unbound() {
        let e = new_env();
        let smb = symbol("foo");
        let evaled = eval(smb, e);

        assert!(evaled.is_err());
        assert_eq!(
            evaled.unwrap_err(),
            SchemeError::UnboundVariable("foo".into())
        );
    }

    #[test]
    fn eval_empty_list() {
        let e = new_env();
        let l = Rc::new(SchemeVal::EmptyList);
        let evaled = eval(l.clone(), e);

        assert_eq!(*evaled.unwrap(), *l);
    }

    #[test]
    fn eval_quoted() {
        let e = new_env();
        let s = symbol("foo");
        let l = list(&[symbol("quote"), s.clone()]);
        let evaled = eval(l, e);

        assert_eq!(*evaled.unwrap(), *s);
    }

    #[test]
    fn eval_definition() {
        let e = new_env();
        let val = number(123.4);

        let a = list(&[symbol("define"), symbol("foo"), val.clone()]);

        let evaled = eval(a, e.clone());
        assert!(evaled.is_ok());

        let new_val = e.borrow().lookup("foo").unwrap();
        assert_eq!(*new_val, *val);
    }

    #[test]
    fn eval_assignment() {
        /*
         * TODO:
         * - set to inner frame
         * - set to outer frame
         * - set to unbound
         */
        let val1 = number(123.4);
        let val2 = number(567.8);
        let val3 = number(901.2);
        let val4 = number(345.6);

        let e_outer = new_env();
        e_outer.borrow_mut().insert("foo".to_string(), val1);

        let e_inner =
            Rc::new(RefCell::new(Environment::new_with_outer(&e_outer)));
        e_inner.borrow_mut().insert("bar".to_string(), val2);

        /* set! to inner frame */
        let test = list(&[symbol("set!"), symbol("bar"), val3.clone()]);

        let evaled = eval(test, e_inner.clone());
        assert!(evaled.is_ok());

        let new_val = e_inner.borrow().lookup("bar").unwrap();
        assert_eq!(*new_val, *val3);

        let new_val_outer = e_outer.borrow().lookup("bar");
        assert!(new_val_outer.is_err());
        assert_eq!(
            new_val_outer.unwrap_err(),
            SchemeError::UnboundVariable("bar".into())
        );

        /* set! to outer frame */
        let test = list(&[symbol("set!"), symbol("foo"), val4.clone()]);

        let evaled = eval(test, e_inner.clone());
        assert!(evaled.is_ok());

        let new_val = e_inner.borrow().lookup("foo").unwrap();
        assert_eq!(*new_val, *val4);

        let new_val_inner = e_inner.borrow().lookup_only("foo");
        assert!(new_val_inner.is_err());
        assert_eq!(
            new_val_inner.unwrap_err(),
            SchemeError::UnboundVariable("foo".into())
        );

        /* set! to unbound */
        let test = list(&[symbol("set!"), symbol("fizz"), val4]);

        let evaled = eval(test, e_inner);
        assert!(evaled.is_err());
        assert_eq!(
            evaled.unwrap_err(),
            SchemeError::UnboundVariable("fizz".into())
        );
    }

    #[test]
    fn eval_if() {
        let e = new_env();
        let val1 = number(1.0);
        let val2 = number(2.0);

        let test =
            list(&[symbol("if"), Rc::new(SchemeVal::True), val1.clone()]);

        let evaled = eval(test, e.clone());
        assert_eq!(*evaled.unwrap(), *val1);

        let test = list(&[
            symbol("if"),
            Rc::new(SchemeVal::False),
            val1,
            val2.clone(),
        ]);

        let evaled = eval(test, e);
        assert_eq!(*evaled.unwrap(), *val2);
    }

    #[test]
    fn eval_lambda() {
        let e = new_env();

        let test = list(&[
            symbol("lambda"),
            list(&[symbol("x"), symbol("y")]),
            list(&[symbol("+"), symbol("x"), symbol("y")]),
        ]);

        let evaled = eval(test, e).unwrap();
        match &*evaled {
            SchemeVal::Procedure(p) => {
                assert_eq!(p.params, vec!["x".to_string(), "y".to_string()]);
                assert_eq!(
                    p.body.car(),
                    list(&[symbol("+"), symbol("x"), symbol("y"),])
                );
            }
            _ => panic!("evaled is not a Procedure"),
        }
    }

    #[test]
    fn eval_begin() {
        let e = new_env();

        let test = list(&[
            symbol("begin"),
            list(&[symbol("quote"), symbol("x")]),
            list(&[symbol("quote"), symbol("y")]),
        ]);

        let evaled = eval(test, e).unwrap();
        assert_eq!(*evaled, *symbol("y"));
    }

    #[test]
    fn eval_application() {
        let e = new_root_env();

        let test = list(&[
            list(&[
                symbol("lambda"),
                list(&[symbol("x"), symbol("y")]),
                list(&[symbol("+"), symbol("x"), symbol("y")]),
            ]),
            number(1.0),
            number(2.0),
        ]);

        let evaled = eval(test, e).unwrap();
        assert_eq!(evaled, number(3.0));
    }

    #[test]
    fn eval_closure() {
        let e = new_root_env();

        // (define y 2)
        // (define add (lambda (x) (+ x y)))
        // (add 1)
        //
        // expected: 3

        let s1 = list(&[symbol("define"), symbol("y"), number(2.0)]);
        let _ = eval(s1, e.clone()).unwrap();

        let s2 = list(&[
            symbol("define"),
            symbol("add"),
            list(&[
                symbol("lambda"),
                list(&[symbol("x")]),
                list(&[symbol("+"), symbol("x"), symbol("y")]),
            ]),
        ]);
        let _ = eval(s2, e.clone()).unwrap();

        let s3 = list(&[symbol("add"), number(1.0)]);
        let evaled = eval(s3, e).unwrap();
        assert_eq!(evaled, number(3.0));
    }

    #[test]
    fn eval_stuff() {
        let e = new_root_env();

        // (define y 1)
        // (define foo
        //   (lambda (x)
        //     (define y 2)
        //     (+ y x)))
        // (foo 3)
        //
        // expected: 5

        let s1 = list(&[symbol("define"), symbol("y"), number(1.0)]);
        let _ = eval(s1, e.clone()).unwrap();

        let s2 = list(&[
            symbol("define"),
            symbol("foo"),
            list(&[
                symbol("lambda"),
                list(&[symbol("x")]),
                list(&[symbol("define"), symbol("y"), number(2.0)]),
                list(&[symbol("+"), symbol("y"), symbol("x")]),
            ]),
        ]);
        let _ = eval(s2, e.clone()).unwrap();

        let s3 = list(&[symbol("foo"), number(3.0)]);
        let evaled = eval(s3, e).unwrap();
        assert_eq!(evaled, number(5.0));
    }

    #[test]
    fn end_to_end1() {
        let e = new_root_env();

        let input = r#"
            (define y 1)
            (define foo
              (lambda (x)
                (define y 2)
                (+ y x)))
            (foo 3)
            "#;

        let expected = number(5.0);

        let mut parser = Parser::new(Lexer::new(input));
        let parsed = parser.parse().unwrap();

        let mut result: Option<Rc<SchemeVal>> = None;
        for stmt in parsed {
            result = Some(eval(stmt, e.clone()).unwrap());
        }

        assert_eq!(result.unwrap(), expected);
    }

    #[test]
    fn end_to_end2() {
        let e = new_root_env();

        let input = r#"
            (define factorial
              (lambda (n)
                (if (= n 1)
                    1
                    (* (factorial (- n 1)) n))))
            (factorial 6)
            "#;

        let expected = number(720.0);

        let mut parser = Parser::new(Lexer::new(input));
        let parsed = parser.parse().unwrap();

        let mut result: Option<Rc<SchemeVal>> = None;
        for stmt in parsed {
            result = Some(eval(stmt, e.clone()).unwrap());
        }

        assert_eq!(result.unwrap(), expected);
    }

    #[test]
    fn end_to_end3() {
        let e = new_root_env();

        let input = r#"
            (((lambda (x y)
                (lambda (a b c d e)
                  ((lambda (f z) (* x (* f z)))
                   (* (* a b) x)
                   (+ (+ c d) x))))
              3
              4)
             1 3 3 4 5)
            "#;

        let expected = number(270.0);

        let mut parser = Parser::new(Lexer::new(input));
        let parsed = parser.parse().unwrap();

        let mut result: Option<Rc<SchemeVal>> = None;
        for stmt in parsed {
            result = Some(eval(stmt, e.clone()).unwrap());
        }

        assert_eq!(result.unwrap(), expected);
    }
}
