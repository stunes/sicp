use std::iter::Peekable;
use std::str::Chars;

#[derive(PartialEq, Debug, Clone)]
pub enum Token {
    Illegal,

    Ident(String),
    Str(String),
    Num(f64),

    LParen,
    RParen,
}

#[derive(Debug)]
pub struct Lexer<'a> {
    input: Peekable<Chars<'a>>,
}

impl<'a> Lexer<'a> {
    pub fn new(input: &'a str) -> Self {
        Lexer {
            input: input.chars().peekable(),
        }
    }

    fn read_string(&mut self) -> String {
        let mut s = String::new();
        while self.input.peek().map_or(false, |c| c != &'"') {
            if let Some(c) = self.input.next() {
                s.push(c);
            }
        }
        self.input.next(); // consume '"'
        s
    }

    fn read_identifier(&mut self, first: char) -> String {
        let mut s = String::new();
        s.push(first);
        while self.input.peek().map_or(false, |c| is_ident_char(*c)) {
            if let Some(c) = self.input.next() {
                s.push(c);
            }
        }
        s
    }

    fn read_number(&mut self, first: char) -> f64 {
        let mut s = String::new();
        s.push(first);
        while self
            .input
            .peek()
            .map_or(false, |c| c.is_digit(10) || *c == '.')
        {
            if let Some(c) = self.input.next() {
                s.push(c);
            }
        }
        s.parse().unwrap()
    }

    fn skip_whitespace(&mut self) {
        while self.input.peek().map_or(false, |c| c.is_whitespace()) {
            self.input.next();
        }
    }
}

fn is_ident_char(c: char) -> bool {
    !(matches!(c, '(' | ')' | '\'') || c.is_whitespace())
}

impl<'a> Iterator for Lexer<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        self.skip_whitespace();

        self.input.next().map(|c| {
            match c {
                '(' => Token::LParen,
                ')' => Token::RParen,
                '"' => Token::Str(self.read_string()),
                _ => {
                    if c.is_numeric() || c == '.' {
                        /* TODO: unary '-' for numeric literals */
                        Token::Num(self.read_number(c))
                    } else if is_ident_char(c) {
                        Token::Ident(self.read_identifier(c))
                    } else {
                        Token::Illegal
                    }
                }
            }
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn lex() {
        let input = r#"
            (define y 2)
            (define add (lambda (x) (+ x y)))
            (add 1)
            (%#foo)
            "#;

        let expected = vec![
            /* (define y 2) */
            Token::LParen,
            Token::Ident("define".to_string()),
            Token::Ident("y".to_string()),
            Token::Num(2.0),
            Token::RParen,
            /* (define add (lambda (x) (+ x y))) */
            Token::LParen,
            Token::Ident("define".to_string()),
            Token::Ident("add".to_string()),
            Token::LParen,
            Token::Ident("lambda".to_string()),
            Token::LParen,
            Token::Ident("x".to_string()),
            Token::RParen,
            Token::LParen,
            Token::Ident("+".to_string()),
            Token::Ident("x".to_string()),
            Token::Ident("y".to_string()),
            Token::RParen,
            Token::RParen,
            Token::RParen,
            /* (add 1) */
            Token::LParen,
            Token::Ident("add".to_string()),
            Token::Num(1.0),
            Token::RParen,
            /* (%#foo) */
            Token::LParen,
            Token::Ident("%#foo".to_string()),
            Token::RParen,
        ];

        let lexer = Lexer::new(input);
        let actual: Vec<Token> = lexer.collect();
        assert_eq!(expected, actual);
    }
}
