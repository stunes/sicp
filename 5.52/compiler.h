typedef enum {
    NUMBER,
} valueType;

typedef struct {
    valueType type;
    union {
        double number;
    } val;
} schemeValue;

typedef struct {
    schemeValue val;
} schemeReg;

void Reg_AssignNumber(schemeReg *, double);
void Reg_AssignString(schemeReg *, const char *);
void *Reg_AsLabel(schemeReg *);

void Stack_Push(schemeReg *);
void Stack_Pop(schemeReg *);

extern schemeReg rg_continue;
extern schemeReg rg_val;
