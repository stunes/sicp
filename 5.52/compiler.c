#include <stdio.h>

#include "compiler.h"

void program(void);

schemeReg rg_val;


void Reg_AssignNumber(schemeReg *reg, double n) {
    reg->val.type = NUMBER;
    reg->val.val.number = n;
}

void Reg_Print(schemeReg *reg) {
    switch (reg->val.type) {
    case NUMBER:
        printf("%f\n", reg->val.val.number);
        break;
    }
}

int main(int argc, char *argv[]) {
    program();

    Reg_Print(&rg_val);
}
